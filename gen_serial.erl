% Copyright (c) 2009, Robert W. Johnstone
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or 
% without modification, are permitted provided that the 
% following conditions are met:
% 
%     * Redistributions of source code must retain the above 
%     copyright notice, this list of conditions and the 
%     following disclaimer.
%     * Redistributions in binary form must reproduce the 
%     above copyright notice, this list of conditions and the 
%     following disclaimer in the documentation and/or other 
%     materials provided with the distribution.
%     * Neither the name of the University of Alberta nor the 
%     names of its contributors may be used to endorse or 
%     promote products derived from this software without 
%     specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
% CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
% INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
% NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
% OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%% @author Robert W. Johnstone
%% @copyright 2009 Robert W. Johnstone
%% @doc Provides a gen_serial module for communicating with 
%% RS-232 serial devices from Erlang. The interface provided 
%% is similar to the gen_tcp module distributed with Erlang.

-module(gen_serial).
-export([start/0, start/1, connect/2, close/1, send/2, recv/2]).

-define(SHARED_LIB,"gen_serial").

-record(comm_port,
    {port, baud_rate=9600, data_bits=8, parity=no_parity, stop_bits=1, flow_control=none} ).

%% @equiv start( "." )
start() ->
	start( "." ).

%% @doc Ensures that the shared library containing the RS-232 port driver is
%% loaded.
%% @spec start( Path ) -> ok | {error, ErrorDesc}
%%    Path = string() | atom()
%%    ErrorDesc = term()
%% @see erl_ddll:load_driver/2
start(Path) ->
    case erl_ddll:load_driver(Path, ?SHARED_LIB) of
        ok -> ok;
        {error, already_loaded} -> ok;
        _Other ->  {error, could_not_load_driver}
    end.

%% @doc Open and configure a RS-232 port as an Erlang port.
%% @spec connect( PortName, Options ) -> {ok, port()} | {ok, ErrorDesc}
%%    PortName = integer()
%%    Options = [OptionItem]
connect( PortName, Options ) when is_integer(PortName) ->
	Rec = get_options( #comm_port{ port=PortName }, Options ),
	connect( Rec );
connect( _PortName, _Options ) ->
	{ error, unsupported }.
	
connect( Rec ) ->
	% Extract and encode parameters
	CommPort = Rec#comm_port.port,
	BaudRate = Rec#comm_port.baud_rate div 300,
	DataBits = Rec#comm_port.data_bits,
	Parity = encode_parity( Rec#comm_port.parity ),
	StopBits = encode_stop_bits( Rec#comm_port.stop_bits ),
	FlowControl = encode_flow_control( Rec#comm_port.flow_control ),
	% Construct binary
	Bin = << CommPort:8, BaudRate:16, DataBits:8, Parity:8, StopBits:8, FlowControl:8 >>,
	% Open shared library port and then open RS232 port
	Port = open_port({spawn, ?SHARED_LIB}, []),
	Res = port_control( Port, 1, Bin ),
	case Res of
		[128] -> { ok, Port };
		[X] -> { error, decode_error_code( X ) };
		_ -> { error, unknown }
	end.
	
encode_parity( P ) ->
	case P of
		none -> 0;
		no_parity -> 0;
		odd -> 1;
		even -> 2;
		mark -> 3;
		space -> 4
	end.
	
encode_stop_bits( P ) ->
	case P of
		1 -> 0;
		1.5 -> 1;
		2 -> 2
	end.
	
encode_flow_control( P ) ->
	case P of 
		none -> 0;
		rts_cts -> 1;
		dtr_dsr -> 2;
		xon_xoff -> 3
	end.

decode_error_code( Code ) ->
	case Code of 
		160 -> cant_open_port;
		161 -> cant_write;
		162 -> cant_read
	end.
	
get_options( Rec, [] ) ->
	Rec;
get_options( Rec, [ {baud_rate,N} | Options ] ) ->
	get_options( Rec#comm_port{ baud_rate=N }, Options );
get_options( Rec, [ {data_bits,N} | Options ] ) ->
	get_options( Rec#comm_port{ data_bits=N }, Options );
get_options( Rec, [ {parity,N} | Options ] ) ->
	get_options( Rec#comm_port{ parity=N }, Options );
get_options( Rec, [ {stop_bits,N} | Options ] ) ->
	get_options( Rec#comm_port{ stop_bits=N }, Options );
get_options( Rec, [ {flow_control,N} | Options ] ) ->
	get_options( Rec#comm_port{ flow_control=N }, Options ).

%% @doc Send data over the RS-232 port.
%% @spec send( port(), Packet ) -> ok | {error, ErrorDesc}
%%   Packet = iolist() | binary()
%%   ErrorDesc = term()
send( Port, Packet ) when is_list( Packet ) ->
	send( Port, list_to_binary( Packet ) );
send( Port, Packet ) when is_binary( Packet ) ->
	port_command( Port, Packet ),
	receive 
		{Port, {data, Data}} ->
			case Data of
				[128] -> ok;
				[X] -> { error, decode_error_code( X ) };
				_ -> { error, unknown }
			end
	end.

%% @doc Close the RS-232 port.
%% @spec close( port() ) -> ok | {error, ErrorDesc}
%%   ErrorDesc = term()
%% @see erlang:port_close/1
close( Port ) when is_port( Port ) ->
	Port ! { self(), close },
	receive
		{ Port, closed } -> ok;
		_ -> { error, unknown }
	end.
	
	
%% @doc Receive data from the RS-232 port.
%% @spec recv( port(), Length ) -> {ok, Packet} | {error, ErrorDesc}
%%   Length = integer()
%%   Packet = iolist()
%%   ErrorDesc = term()
recv( Port, 0 ) ->
	Res = port_control( Port, 3, [] ),
	case Res of
		[128 | Packet] -> { ok, Packet };
		[X] -> { error, decode_error_code( X ) };
		_ -> Res
	end;
recv( Port, Length ) when is_integer(Length) ->
	Res = port_control( Port, 4, << Length:32 >> ),
	case Res of
		[128 | Packet ] ->
			recv_build( [Packet], Port, Length-length(Packet) );
		[X] -> { error, decode_error_code( X ) };
		_ -> Res
	end.
	
recv_build( Packets, Port, Length ) ->
	if 
		Length > 0 ->
			Res = port_control( Port, 4, << Length:32 >> ),
			case Res of
				[ 128 | NewPacket ] ->
					recv_build( [ NewPacket | Packets ], Port, Length );
				[ X ] -> 
					{ error, decode_error_code( X ) };
				_ -> 
					Res
			end;
		true ->
			{ ok, Packets }
	end.
