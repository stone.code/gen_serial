c( gen_serial ).

gen_serial:start( "./Debug" ).
{ ok, Port } = gen_serial:connect( 4, [{flow_control, dtr_dsr}] ).
gen_serial:send( Port, <<"*IDN?\n">> ).
gen_serial:recv( Port, 0 ).
gen_serial:close( Port ).
